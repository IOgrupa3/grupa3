package Data;

/**
 * Created by User on 2016-11-30.
 */
public class Grupa {
    public int ID;
    public String nazwa;
    public int rodzaj_grupy;

    public Grupa(int ID , String nazwa, int t)
    {
        this.ID=ID;
        this.nazwa=nazwa;
        this.rodzaj_grupy=t;
    }

    public String getNazwa() {
        return nazwa;
    }

    public int getID() {
        return ID;
    }

    public int getRodzaj_grupy() {
        return rodzaj_grupy;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    public void setRodzaj_grupy(int rodzaj_grupy) {
        this.rodzaj_grupy = rodzaj_grupy;
    }
}
