package Data;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * Created by User on 2016-11-30.
 */
public class Genarotor {

    public static Plan_zajec generuj(List<Grupa> list_grup,List<Sala> list_sal,List<Prowadzacy> list_pro)
    {
        Plan_zajec nowy_plan = new Plan_zajec();

        for (Grupa grupa : list_grup)
        {
            for(Prowadzacy prow : list_pro) {
                if(grupa.getRodzaj_grupy()==prow.getRodzaj_zajec()) {
                    Zajecia nowe = new Zajecia();
                    nowe.setGrupa(grupa);
                    nowe.setProwadzacy(prow);
                    nowe.setSala(findSala(list_sal,grupa.getRodzaj_grupy()));
                    wpiszDoPlanu(nowy_plan,findMaxPreferencje(prow.getPreferencje()),nowe);
                    //nowy_plan.getPlan()[findMaxPreferencje(prow.getPreferencje())][0]=nowe;
                }
            }

        }




        nowy_plan.getPlan();

        return nowy_plan;
    }

    static Sala findSala(List<Sala> list_sal, int rodzaj)
    {
        for (Sala sala: list_sal) {
            if(sala.getTyp_sali()==rodzaj)
            {
                return sala;
            }

        }
        return null;
    }

    static int findMaxPreferencje(int[] tab)
    {
        int max = tab[0];
        int id=0;
        for (int i = 0; i < tab.length; i++) {
            if (tab[i] > max) {
                max = tab[i];
                id=i;
            }
        }
        return id;
    }

    static void wpiszDoPlanu(Plan_zajec nowy_plan,int i,Zajecia nowe)
    {
        for(int j=0;i<nowy_plan.getPlan()[i].length;j++)
        {
            if(nowy_plan.getPlan()[i][j].isEmpty()==false)
            {
                nowy_plan.getPlan()[i][j]=nowe;
                nowy_plan.getPlan()[i][j].setEmpty(true);
                return;
            }
        }
    }
}
