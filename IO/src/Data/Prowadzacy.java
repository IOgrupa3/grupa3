package Data;

/**
 * Created by User on 2016-11-30.
 */
public class Prowadzacy {
    public int ID;
    public String imie;
    public String nazwisko;
    public String przedmiot;
    public int rodzaj_zajec;
    public int [] preferencje;

    public Prowadzacy(int ID, String imie, String nazwisko, String przedmiot, int [] preferencje, int rodzaj_zajec)
    {
        this.ID=ID;
        this.imie=imie;
        this.nazwisko=nazwisko;
        this.przedmiot=przedmiot;
        this.preferencje=preferencje;
        this.rodzaj_zajec = rodzaj_zajec;
    }


    public int getID() {
        return ID;
    }

    public String getImie() {
        return imie;
    }

    public int[] getPreferencje() {
        return preferencje;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public String getPrzedmiot() {
        return przedmiot;
    }

    public int getRodzaj_zajec() {
        return rodzaj_zajec;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }

    public void setPreferencje(int[] preferencje) {
        this.preferencje = preferencje;
    }

    public void setPrzedmiot(String przedmiot) {
        this.przedmiot = przedmiot;
    }

    public void setRodzaj_zajec(int rodzaj_zajec) {
        this.rodzaj_zajec = rodzaj_zajec;
    }
}
