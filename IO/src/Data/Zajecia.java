package Data;

/**
 * Created by User on 2016-11-30.
 */
public class Zajecia {
    boolean empty;
    public Grupa grupa;
    public Prowadzacy prowadzacy;
    public Sala sala;

    public Zajecia(Grupa g, Prowadzacy p, Sala s)
    {
        this.grupa=g;
        this.prowadzacy=p;
        this.sala=s;
        empty = false;
    }

    public Zajecia()
    {
        this.empty = false;
    }

    public boolean isEmpty() {
        return empty;
    }

    public Grupa getGrupa() {
        return grupa;
    }

    public Prowadzacy getProwadzacy() {
        return prowadzacy;
    }

    public Sala getSala() {
        return sala;
    }

    public void setGrupa(Grupa grupa) {
        this.grupa = grupa;
    }

    public void setProwadzacy(Prowadzacy prowadzacy) {
        this.prowadzacy = prowadzacy;
    }

    public void setSala(Sala sala) {
        this.sala = sala;
    }

    public void setEmpty(boolean empty) {
        this.empty = empty;
    }
}
