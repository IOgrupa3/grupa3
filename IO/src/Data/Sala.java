package Data;

/**
 * Created by User on 2016-11-30.
 */
public class Sala {
    public int ID;
    public String nazwa;
    public int typ_sali;

    public Sala( int ID, String nazwa, int typ_sali)
    {
        this.ID=ID;
        this.nazwa=nazwa;
        this.typ_sali=typ_sali;
    }


    public int getID() {
        return ID;
    }

    public String getNazwa() {
        return nazwa;
    }

    public int getTyp_sali() {
        return typ_sali;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    public void setTyp_sali(int typ_sali) {
        this.typ_sali = typ_sali;
    }
}
