package com.company;

import Data.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Main {

    public static void main(String[] args) {
	    List<Grupa> list_grup = new ArrayList<Grupa>();
        List<Sala> list_sal = new ArrayList<Sala>();
        List<Prowadzacy> list_pro = new ArrayList<Prowadzacy>();


        list_grup.add(new Grupa(1, "GR_W1", TYP_ZAJEC.WYKLAD));
        list_grup.add(new Grupa(2, "GR_c1", TYP_ZAJEC.CWICZENIA));
        list_grup.add(new Grupa(3, "GR_c2", TYP_ZAJEC.CWICZENIA));

        int [] p = new int[5];
        Random rand = new Random();
        for (int i=0;i<5;i++)
        {

            p[i] = rand.nextInt(100);
        }

        list_pro.add(new Prowadzacy(1,"Florian","Strama","podstawy",p, TYP_ZAJEC.WYKLAD ));
        list_pro.add(new Prowadzacy(1,"Florian","Strama","podstawy",p, TYP_ZAJEC.CWICZENIA ));
        int [] t = new int[5];
        for (int i=0;i<5;i++)
        {

            t[i] = rand.nextInt(100);
        }
        list_pro.add(new Prowadzacy(2,"Ktos","Strama","II",t , TYP_ZAJEC.WYKLAD ));
        list_pro.add(new Prowadzacy(2,"Ktos","Strama","II",t , TYP_ZAJEC.LABORATORIA ));

        list_sal.add(new Sala(1,"B-4 201",TYP_ZAJEC.WYKLAD));
        list_sal.add(new Sala(2,"B-4 202",TYP_ZAJEC.CWICZENIA));
        list_sal.add(new Sala(3,"B-4 203",TYP_ZAJEC.CWICZENIA));
        list_sal.add(new Sala(4,"B-4 203",TYP_ZAJEC.LABORATORIA));

        Plan_zajec nowy=Genarotor.generuj(list_grup,list_sal,list_pro);

        nowy.getPlan();
    }
}
